## Juego del ahorcado

Programa para practicar habilidades en Python. Versión sencilla de un juego de ahorcado en Python. Créditos para el canal freeCodeCamp en español y su [video](https://www.youtube.com/watch?v=tWnyBD2src0&t=4519s)

NOTA: A pesar de que me inspiré en ese video para hacer el juego del ahorcado y estoy empleando instrucciones que se usan ahí, me empeñé en no copiar el código tal cual, de modo que parezca que es una versión mía.

### Registro de actividades

**23/04/2022** 
- Creados archivos iniciales. 
- Primer prototipo funcionando.
- Código en bucle infinito.

**26/04/2022**
- Creado cuaderno Jupyter Notebook para crear una versión en TkInter.

**02/05/2022**
- Añadido código para calificar al usuario en función del porcentaje restante de intentos con respecto al largo de la palabra a adivinar. Está escrito para facilitar posteriormente el guardar esos datos en una BD.
- El programa ya no admite ingresos de más de una letra o ninguna letra.
- Bloques condicionales reacomodados dentro del bucle infinito.

**03/05/2022**
- Añadida BD en Sqlite3 para los puntajes.

**04/05/2022**
- El programa muestra los 10 resultados más recientes, para evitar llenar la pantalla si es que hay muchos. Se muestra el último al principio, luego el penúltimo segundo, y así sustantivamente.

**11/05/2022**
- Se ha creado una lista donde se añadirán todas las letras que han sido utilizadas, tanto aciertos como fallas. De ese modo, el usuario no perderá un intento por haber repetido una letra ingresada anteriormente.

**13/05/2022**
- Creado el primer prototipo funcional del código en TkInter. Créditos al autor en [este video](https://www.youtube.com/watch?v=4FE5MjKvweo) porque de ahí tomé la idea de usar los guiones y el Canvas, y de ahí aprendí a poner imágenes.
