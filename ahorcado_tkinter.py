import random
from tkinter import *
import os
import sys
from PIL import ImageTk

from palabras import palabras

#Reiniciar programa

def reiniciarPrograma():
    reinicio = sys.executable
    os.execl(reinicio, reinicio, * sys.argv)
    

#Generar aleatoriamente la palabra

def obtenerPalabra():
    palabra = random.choice(palabras)
    return palabra

# Carga de imágenes

def cargarImagen(intentos):
    if (intentos < (len(palabra) * 2) * 0.8 and
        intentos >= (len(palabra) * 2) * 0.6):
        canvas.itemconfig(imagen_id, image=imagenes[1])
        
    elif (intentos < (len(palabra) * 2) * 0.6 and
        intentos >= (len(palabra) * 2) * 0.4):
        canvas.itemconfig(imagen_id, image=imagenes[2])
        
    elif (intentos < (len(palabra) * 2) * 0.4 and
        intentos >= (len(palabra) * 2) * 0.2):
        canvas.itemconfig(imagen_id, image=imagenes[3])
        
    elif intentos == 0 and len(letras_restantes) > 0:
        canvas.itemconfig(imagen_id, image=imagenes[4])
        

# Condiciones de victoria o derrota

def final():
    
    # Jugador Gana
    
    if len(letras_restantes) == 0:
        l_final.configure(text="GANASTE", fg = "green", font=(25))        
        
        # Calificación al jugador
                    
        if intentos <= len(palabra) * 0.3:
            calificacion = "El Horror"
            l_calificacion.configure(text=f"Calificación: {calificacion}")
                        
                                   
        elif intentos > len(palabra) * 0.3 and intentos <= len(palabra) * 0.6:
            calificacion = "Decente"
            l_calificacion.configure(text=f"Calificación: {calificacion}")
                        
        else:
            calificacion = "Experto(a)"
            l_calificacion.configure(text=f"Calificación: {calificacion}")
    
    #Jugador pierde
    
    elif len(letras_restantes) > 0 and intentos == 0:
        l_final.configure(text="PERDISTE", fg = "red", font=(25))
        l_calificacion.configure(text=f"La palabra era {palabra}")
        
    btn_reiniciar.grid(row=8, column=0)


# Pulsación del botón

def pulsacion():
    global intentos
    global letras_usadas
    letra = probarLetra.get().upper()    
        
    if len(letra) != 1 or letra not in abecedario:
        l.configure(text="Ingreso inválido",fg="black")
        ent_letra.delete(0, END)
        
    elif letra in letras_usadas:
        l.configure(text="Letra ya usada antes", fg="black")
        ent_letra.delete(0, END)        
    
    elif letra in palabra:
        l.configure(text="¡ACIERTO!", fg="green")
        letras_usadas.append(letra)
        letras_restantes.remove(letra)
        for i in range(len(guiones)):
            if letra in palabra[i]:
                guiones[i] = letra
                l_palabra.configure(text = guiones)
                
        ent_letra.delete(0, END)
        intentos -= 1
        l_intentos.configure(text=f"{intentos} intento(s) restante(s)")
        cargarImagen(intentos)
        
        
    elif letra not in palabra:
        l.configure(text="¡FALLASTE!", fg="red")
        letras_usadas.append(letra)
        ent_letra.delete(0, END)
        intentos -= 1
        l_intentos.configure(text=f"{intentos} intento(s) restante(s)")
        cargarImagen(intentos)
        
    if len(letras_restantes) == 0 or intentos == 0:
        final()
        

#Inicializar la interfaz gráfica


root = Tk()
root.geometry("400x320")
#root.config(width = 400, height = 320, relief = "sunken", bd = 10)
root.title("Juego del ahorcado")
canvas = Canvas(root, width = 400, height = 320)
canvas.pack(expand = True, fill="both")

abecedario = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"

probarLetra = StringVar()

letras_usadas = []

guiones = []

palabra = obtenerPalabra()

letras_restantes = set(palabra)

for i in range(len(palabra)):
    guiones.append("_")
    
print(guiones)

aciertos = []

intentos = len(palabra) * 2

imagenes = [
    PhotoImage(file="100.png"),
    PhotoImage(file="75.png"),
    PhotoImage(file="50.png"),
    PhotoImage(file="25.png"),
    PhotoImage(file="0.png")
]

# Widgets

imagen_id = canvas.create_image(300,130, image=imagenes[0])

l_ingresar = Label(canvas, text="Ingresa una letra")
l_ingresar.grid(row=3, column=0)

ent_letra = Entry(canvas, width = 1, textvariable = probarLetra)
ent_letra.grid(row=3, column=1)
ent_letra.focus()

btn1 = Button(canvas, text = "Ingresar", pady = 3, padx = 5,
              command = lambda:pulsacion())
btn1.grid(row=4, column=0)

btn_reiniciar = Button(canvas, text = "Reiniciar", bg = "#014760",
                       fg = "white", command = reiniciarPrograma)

l = Label(canvas)
l.grid(row=5, column=0, pady=10)


l_palabra = Label(canvas, text = guiones, font=("Verdana", 20), padx=10, pady=10)
l_palabra.grid(row=0, column=0, pady=10)

l_intentos = Label(canvas, text= f"{intentos} intento(s) restante(s)")
l_intentos.grid(row=1, column=0, padx=10, pady=10)

l_final = Label(canvas)
l_final.grid(row=6, column=0)

l_calificacion = Label(canvas)
l_calificacion.grid(row=7, column=0)

mainloop()